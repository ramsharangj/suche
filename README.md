# Requirements for the project

* Ruby
* Bundler

# Setting up the project

* Run `bundle install`
* Run `bundle exec rspec` for running tests
* Run `bundle exec rackup` to start the web server

# Running with different source code

* Stop web server if running
* Change the `FILENAME` variable in application.rb to the new source code path. Do not add '/' in the end.
* Start web serve again

# API Documentation

## Search

Request: `GET http://localhost:9292/search?q=search_term`

Required Parameters:
`q`: the search term that you need

Response:
```
{
  name: filename,
  search_results: [result1, result2],
  count: 2
}
```

The link for the bitbucket project is as follows: `https://bitbucket.org/ramsharangj/suche`
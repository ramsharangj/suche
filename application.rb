# encoding: UTF-8
require 'sinatra'
require 'sinatra/json'
require './models/cache'
require './index/load_data'

FILENAME = "./db/redis-unstable"
CACHE = Cache.new # Initialize cache
include LoadData

# Load Data into cache
iterate_files(FILENAME)

class Application < Sinatra::Base
  #Search cache
  get '/search' do
    if params[:q]
      json CACHE.search(params[:q])
    else
      raise "Invalid params: q is required"
    end
  end

  run! if __FILE__ == $0
end

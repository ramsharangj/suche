# encoding: UTF-8
require 'byebug'
class Cache
  attr_reader :data

  def initialize
    @data = []
  end

  def add(hash={})
    data.push(hash) unless hash.empty?
  end

  def search(string='')
    if string != ''
      data.map do |hash|
        matched_contents = hash[:contents].scan(/(.*#{string}.*)/)
        if matched_contents.count > 0
          {
            name: hash[:name],
            search_results: matched_contents.flatten,
            count: matched_contents.count
          }
        end
      end.compact
    end
  end
end

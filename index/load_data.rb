module LoadData
  def iterate_files(file_name)
    Dir.glob(file_name + "/*").each do |element|
      if Dir.exists?(element)
        iterate_files(element)
      else
        filename = File.expand_path(element)
        file = File.open(filename, "rb:UTF-8")
        contents = file.read
        if ! contents.valid_encoding?
          contents = contents.encode("UTF-8", :invalid=>:replace, :replace=>"?").encode('UTF-8')
        end
        CACHE.add(
          {
            name: filename,
            contents: contents
          }
        )
      end
    end
  end
end

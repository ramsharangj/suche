require_relative '../spec_helper'
require_relative './../../models/cache'

describe Cache do
  before(:all) do
    @cache = Cache.new
  end

  it 'displays data from the cache' do
    expect(@cache.data).to be_a(Array)
  end

  it 'adds a document to the cache' do
    @cache.add({name: 'file', contents: 'content'})
    expect(@cache.data.first).to eq({:name=>"file", :contents=>"content"})
  end

  it 'suggests all the documents from the cache' do
    @cache.add({name: 'file', contents: 'content'})
    expect(@cache.search('content').first[:name]).to eq('file')
  end
end
